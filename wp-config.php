<?php

define('APP_ROOT', dirname(__DIR__));
define('APP_ENV', getenv('APPLICATION_ENV'));

define('DB_NAME', 'blueberry_products');
define('DB_USER', 'blueberry_products');
define('DB_PASSWORD', 'gN*r6%VM6*HrG2bg');
$table_prefix = 'bb';
define('DB_HOST', 'blueberry.bg');

define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

define( 'AUTH_KEY',         'p$~+(m&yfb.B)NRkSW^hIav,A*<5ycus$WOV %%|TH!:AmqR#2z;xbho(*C2du^]' );
define( 'SECURE_AUTH_KEY',  'm?(;:bq@3q;KNNh*qhmv|o.NZt&;QG-hX|q>t$.D#V-bhfc3q^8oS[DRYHPj6*kI' );
define( 'LOGGED_IN_KEY',    '/(o|^&$ R 0{rHv}YvEQEe*R7Mo?a{x.A83Xt:7!^:soT{[|[bpZcv?tfD.puXUo' );
define( 'NONCE_KEY',        '?#Qc[}jJWN2{Ei[qOl: u=]Tw{(7#x$6.!Z)EU$m|R`N+yBF`i}Q@X[>&ttmxx>7' );
define( 'AUTH_SALT',        'XovB8A5^M_ [{-,r-WMhUH0!H,[Sen@`kk2lhOM>~mf9MaUBQ3I]ut8$5RNSW#]F' );
define( 'SECURE_AUTH_SALT', 'wi.=wpHKtIsSSpXysObYri9@GWD*{:7<#NCFdgI,2d+VrJA^p}1P5ly*)^Tn}i?_' );
define( 'LOGGED_IN_SALT',   'ir;2TUF,81QK`nc$N$UqJjx|IWKTSC<6o^K!Q`eDh~4A)6SpE.iv$e&A2-S8+8RS' );
define( 'NONCE_SALT',       'U-f1Ib<ZQ3K_~X66s{Y-![B+rj#T({Un:T0{y)&u8zAp1ljb?5$4@OAZh+Th>tM;' );

define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);

define('WP_POST_REVISIONS', 3);

define('WP_HOME', 'http://products.blueberry.bg');
define('WP_SITEURL', 'http://products.blueberry.bg/app');
define('WP_CONTENT_URL', 'http://products.blueberry.bg/content');
define('WP_CONTENT_DIR', dirname(__FILE__) . '/content');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . '/wp-settings.php');
