<?php

if ( ! function_exists( 'foundationpress_gutenberg_support' ) ) :
	function foundationpress_gutenberg_support() {

    add_theme_support( 'align-wide' );
    add_theme_support( 'responsive-embeds' );

    // Add foundation color palette to the editor
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'Primary Color', 'theme_textdomain' ),
            'slug' => 'primary',
            'color' => '#1779ba',
        ),
        array(
            'name' => __( 'Secondary Color', 'theme_textdomain' ),
            'slug' => 'secondary',
            'color' => '#767676',
        ),
        array(
            'name' => __( 'Success Color', 'theme_textdomain' ),
            'slug' => 'success',
            'color' => '#3adb76',
        ),
        array(
            'name' => __( 'Warning color', 'theme_textdomain' ),
            'slug' => 'warning',
            'color' => '#ffae00',
        ),
        array(
            'name' => __( 'Alert color', 'theme_textdomain' ),
            'slug' => 'alert',
            'color' => '#cc4b37',
        )
		) );

		add_theme_support(
			'editor-font-sizes',
			array(
					array(
							'name'      => __( '~H1', 'theme_textdomain' ),
							'shortName' => __( 'H1', 'theme_textdomain' ),
							'size'      => 40,
							'slug'      => 'headingone',
					),
					array(
							'name'      => __( '~H2', 'theme_textdomain' ),
							'shortName' => __( 'H2', 'theme_textdomain' ),
							'size'      => 35,
							'slug'      => 'headingtwo',
					),
					array(
							'name'      => __( '~H3', 'theme_textdomain' ),
							'shortName' => __( 'H3', 'theme_textdomain' ),
							'size'      => 30,
							'slug'      => 'headingthree',
					),
					array(
							'name'      => __( '~H4', 'theme_textdomain' ),
							'shortName' => __( 'H4', 'theme_textdomain' ),
							'size'      => 24,
							'slug'      => 'headingfour',
					),
					array(
							'name'      => __( '~H5', 'theme_textdomain' ),
							'shortName' => __( 'H5', 'theme_textdomain' ),
							'size'      => 22,
							'slug'      => 'headingfive',
					),
					array(
							'name'      => __( '~H6', 'theme_textdomain' ),
							'shortName' => __( 'H6', 'theme_textdomain' ),
							'size'      => 18,
							'slug'      => 'headingsix',
					),
					array(
							'name'      => __( 'Small Text', 'theme_textdomain' ),
							'shortName' => __( 'SM', 'theme_textdomain' ),
							'size'      => 12,
							'slug'      => 'small',
					),
				)
			);
	}

	add_action( 'after_setup_theme', 'foundationpress_gutenberg_support' );
endif;
