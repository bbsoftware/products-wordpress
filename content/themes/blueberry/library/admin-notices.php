<?php

	function author_admin_notice(){
    global $pagenow;
    if ( $pagenow == 'index.php' ) {
    $user = wp_get_current_user();
    if ( in_array( 'administrator', (array) $user->roles ) ) {
		echo '<div class="notice notice-success is-dismissible">
					<h4>Happy coding!</h4>
         </div>';
    }
	}
}
add_action('admin_notices', 'author_admin_notice');

?>
