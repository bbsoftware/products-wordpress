<?php

	// register media tax
	add_action('init', function() {
		register_taxonomy(
				'media-category',
				'attachment'
		);
	});

?>
