/*
    Filters here

    We usually register block styles inside gutenberg.php, but this is
    just an example

*/

// wp.blocks.registerBlockStyle( 'core/heading', {
//     name: 'uppercase-heading',
//     label: 'Uppercase heading'
// } );


window.onload = function(){
  wp.blocks.unregisterBlockType( 'core/archives' );
  wp.blocks.unregisterBlockType( 'core/calendar' );
  wp.blocks.unregisterBlockType( 'core/categories' );
  wp.blocks.unregisterBlockType( 'core/latest-comments' );
  wp.blocks.unregisterBlockType( 'core/latest-posts' );
  wp.blocks.unregisterBlockType( 'core/rss' );
  wp.blocks.unregisterBlockType( 'core/tag-cloud' );


  wp.blocks.unregisterBlockType( 'core-embed/wordpress' );
  wp.blocks.unregisterBlockType( 'core-embed/spotify' );
  wp.blocks.unregisterBlockType( 'core-embed/flickr' );
  wp.blocks.unregisterBlockType( 'core-embed/animoto' );
  wp.blocks.unregisterBlockType( 'core-embed/cloudup' );
  wp.blocks.unregisterBlockType( 'core-embed/collegehumor' );
  wp.blocks.unregisterBlockType( 'core-embed/dailymotion' );
  wp.blocks.unregisterBlockType( 'core-embed/issuu' );
  wp.blocks.unregisterBlockType( 'core-embed/kickstarter' );
  wp.blocks.unregisterBlockType( 'core-embed/meetup-com' );
  wp.blocks.unregisterBlockType( 'core-embed/mixcloud' );
  wp.blocks.unregisterBlockType( 'core-embed/polldaddy' );
  wp.blocks.unregisterBlockType( 'core-embed/reddit' );
  wp.blocks.unregisterBlockType( 'core-embed/reverbnation' );
  wp.blocks.unregisterBlockType( 'core-embed/screencast' );
  wp.blocks.unregisterBlockType( 'core-embed/speaker-deck' );
  wp.blocks.unregisterBlockType( 'core-embed/tiktok' );
  wp.blocks.unregisterBlockType( 'core-embed/crowdsignal' );
  wp.blocks.unregisterBlockType( 'core-embed/amazon-kindle' );
  wp.blocks.unregisterBlockType( 'core-embed/scribd' );
  wp.blocks.unregisterBlockType( 'core-embed/slideshare' );
  wp.blocks.unregisterBlockType( 'core-embed/smugmug' );
  wp.blocks.unregisterBlockType( 'core-embed/speaker' );
  wp.blocks.unregisterBlockType( 'core-embed/ted' );
  wp.blocks.unregisterBlockType( 'core-embed/tumblr' );
  wp.blocks.unregisterBlockType( 'core-embed/videopress' );
  wp.blocks.unregisterBlockType( 'core-embed/wordpress-tv' );
};
