<?php
/**
 * Block Name: Test
 *
 */
   
    // Create class attribute allowing for custom "className" and "align" values.
    $className = 'test';
    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }
    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }

?>

<div class="<?php echo esc_attr($className); ?>">

</div>