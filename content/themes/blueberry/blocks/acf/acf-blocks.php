<?php 

	add_action('acf/init', 'my_acf_init');
	function my_acf_init() {

		// check function exists
		if( function_exists('acf_register_block') ) {
			
			acf_register_block(array(
				'name'				=> 'test',
				'title'				=> __('Test'),
				'description'		=> __('Custom Description'),
				'render_callback'	=> 'my_acf_block_render_callback',
				// 'category'			=> 'custom-category',
				'icon'				=> 'feedback',
				'keywords'			=> array( 'bread'),
				'supports' => array( 'multiple' => true,),

			));
		}
		
	}

	// Render ACF blocks
	function my_acf_block_render_callback( $block ) {
		
		// convert name ("acf/block") into path friendly slug ("block")
		$slug = str_replace('acf/', '', $block['name']);
		
		// include a template part from within the "template-parts/block" folder
		if( file_exists( get_theme_file_path("/blocks/acf/blocks/acfblock-{$slug}.php") ) ) {
			include( get_theme_file_path("/blocks/acf/blocks/acfblock-{$slug}.php") );
		}
	}
