<?php
/**
 * URL: http://hlebarov.com
 *
 * HlebarovPress functions and definitions
 */

/** Various clean up functions */
require_once 'library/cleanup.php';

/** Required for Foundation to work properly */
require_once 'library/foundation.php';

/** Format commentsf */
require_once 'library/class-foundationpress-comments.php';

/** Register all navigation menus */
require_once 'library/navigation.php';

/** Add menu walkers for top-bar and off-canvas */
require_once 'library/class-foundationpress-top-bar-walker.php';
require_once 'library/class-foundationpress-mobile-walker.php';

/** Create widget areas in sidebar and footer */
require_once 'library/widget-areas.php';

/** Enqueue scripts */
require_once 'library/enqueue-scripts.php';

/** Register taxonomies */
require_once 'library/tax.php';

/** Admin notices */
require_once 'library/admin-notices.php';

/** Add theme support */
require_once 'library/theme-support.php';

/** Add Nav Options to Customer */
require_once 'library/custom-nav.php';

/** Change WP's sticky post class */
require_once 'library/sticky-posts.php';

/** Configure responsive image sizes */
require_once 'library/responsive-images.php';

/** Gutenberg editor support */
require_once 'library/gutenberg.php';

/** All things acf */
require_once 'library/acf.php';
