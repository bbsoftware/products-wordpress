import $ from 'jquery';

window.$ = $;

import whatInput from 'what-input';
// import Foundation from 'foundation-sites';

// - Uncomment for slider module
// import Swiper from 'swiper';
// import slick from 'slick-carousel';

/*
    If you want to pick and choose which modules to include, comment out the above and uncomment
    the line below
*/
import './lib/foundation-explicit-pieces';

$(document).foundation();

$(function(){

});
