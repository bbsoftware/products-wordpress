<?php
/*
Template Name: Kitchen Sink
*/
get_header(); ?>


<?php /* Start loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="main-container">
        <div class="main-grid">
            <header class="kitchen-sink-header">
                <h1 class="entry-title"><?php the_title(); ?></h1><hr>
                <p class="lead">This page includes every single Foundation element so that we can make sure things work together smoothly.</p><hr>
            </header>

            <!-- Main wrapper for the components in the kitchen-sink -->
            <div id="components" class="kitchen-sink-components">
                <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                    <!-- Abide -->
                    
                    <hr>

                    <!-- Accordion -->
                    <h2 id="accordion" class="docs-heading" data-magellan-target="accordion"><a href="#accordion"></a>Accordion</h2>
                    <ul class="accordion" data-accordion role="tablist">
                        <li class="accordion-item" data-accordion-item>
                            <!-- The tab title needs role="tab", an href, a unique ID, and aria-controls. -->
                            <a href="#panel1d" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Accordion 1</a>
                            <!-- The content pane needs an ID that matches the above href, role="tabpanel", data-tab-content, and aria-labelledby. -->
                            <div id="panel1d" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
                                Panel 1. Lorem ipsum dolor
                            </div>
                        </li>
                        <li class="accordion-item" data-accordion-item>
                            <!-- The tab title needs role="tab", an href, a unique ID, and aria-controls. -->
                            <a href="#panel1d" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Accordion 2</a>
                            <!-- The content pane needs an ID that matches the above href, role="tabpanel", data-tab-content, and aria-labelledby. -->
                            <div id="panel1d" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
                                Panel 2. Lorem ipsum dolor
                            </div>
                        </li>
                        <li class="accordion-item" data-accordion-item>
                            <!-- The tab title needs role="tab", an href, a unique ID, and aria-controls. -->
                            <a href="#panel1d" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Accordion 3</a>
                            <!-- The content pane needs an ID that matches the above href, role="tabpanel", data-tab-content, and aria-labelledby. -->
                            <div id="panel1d" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
                                Panel 3. Lorem ipsum dolor
                            </div>
                        </li>
                    </ul>
                    <hr>

                    <!-- Accordion Menu -->
                    <h2 id="accordion-menu" class="docs-heading" data-magellan-target="accordion-menu"><a href="#accordion-menu"></a>Accordion Menu</h2>
                    <ul class="vertical menu" data-accordion-menu>
                        <li>
                            <a href="#">Item 1</a>
                            <ul class="menu vertical nested is-active">
                                <li>
                                    <a href="#">Item 1A</a>
                                    <ul class="menu vertical nested">
                                        <li><a href="#">Item 1Ai</a></li>
                                        <li><a href="#">Item 1Aii</a></li>
                                        <li><a href="#">Item 1Aiii</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Item 1B</a></li>
                                <li><a href="#">Item 1C</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Item 2</a>
                            <ul class="menu vertical nested">
                                <li><a href="#">Item 2A</a></li>
                                <li><a href="#">Item 2B</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Item 3</a></li>
                    </ul>
                    <hr>

                    <!-- Button -->
                    <h2 id="button" class="docs-heading" data-magellan-target="button"><a href="#button"></a>Button</h2>
                        <a class="button primary" href="#">Primary</a>
                        <a class="button secondary" href="#">Secondary</a>
                        <a class="button success" href="#">Success</a>
                        <a class="button alert" href="#">Alert</a>
                        <a class="button warning" href="#">Warning</a>

                        <br />
                        <!-- Buttons (actions) -->
                        <button type="button" class="success button">Save</button>
                        <button type="button" class="alert button">Delete</button>

                        <br />
                        <a class="tiny button" href="#">So Tiny</a>
                        <a class="small button" href="#">So Small</a>
                        <a class="button" href="#">So Basic</a>
                        <a class="large button" href="#">So Large</a>
                        <a class="expanded button" href="#">Such Expand</a>

                        <div class="button-group">
                            <a class="button">One</a>
                            <a class="button">Two</a>
                            <a class="button">Three</a>
                        </div>
                    <hr>

            
                    <!-- Drilldown Menu -->
                    <h2 id="drilldown-menu" class="docs-heading" data-magellan-target="drilldown-menu"><a href="#drilldown-menu"></a>Drilldown Menu</h2>
                        <ul class="vertical menu" data-drilldown style="width: 200px" id="m1">
                            <li>
                                <a href="#">Item 1</a>
                                <ul class="vertical menu" id="m2">
                                    <li>
                                        <a href="#">Item 1A</a>
                                        <ul class="vertical menu" id="m3">
                                            <li><a href="#">Item 1Aa</a></li>
                                            <li><a href="#">Item 1Ba</a></li>
                                            <li><a href="#">Item 1Ca</a></li>
                                            <li><a href="#">Item 1Da</a></li>
                                            <li><a href="#">Item 1Ea</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Item 1B</a></li>
                                    <li><a href="#">Item 1C</a></li>
                                    <li><a href="#">Item 1D</a></li>
                                    <li><a href="#">Item 1E</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Item 2</a>
                                <ul class="vertical menu">
                                    <li><a href="#">Item 2A</a></li>
                                    <li><a href="#">Item 2B</a></li>
                                    <li><a href="#">Item 2C</a></li>
                                    <li><a href="#">Item 2D</a></li>
                                    <li><a href="#">Item 2E</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Item 3</a>
                                <ul class="vertical menu">
                                    <li><a href="#">Item 3A</a></li>
                                    <li><a href="#">Item 3B</a></li>
                                    <li><a href="#">Item 3C</a></li>
                                    <li><a href="#">Item 3D</a></li>
                                    <li><a href="#">Item 3E</a></li>
                                </ul>
                            </li>
                            <li><a href='#'> Item 4</a></li>
                        </ul>
                    <hr>

                    <!-- Dropdown Menu -->
                    <h2 id="dropdown-menu" class="docs-heading" data-magellan-target="dropdown-menu"><a href="#dropdown-menu"></a>Dropdown Menu</h2>
                        <ul class="dropdown menu" data-dropdown-menu>
                            <li>
                                <a>Item 1</a>
                                <ul class="menu">
                                    <li><a href="#">Item 1A Loooong</a></li>
                                    <li>
                                        <a href='#'> Item 1 sub</a>
                                        <ul class='menu'>
                                            <li><a href='#'>Item 1 subA</a></li>
                                            <li><a href='#'>Item 1 subB</a></li>
                                            <li>
                                            <a href='#'> Item 1 sub</a>
                                            <ul class='menu'>
                                                <li><a href='#'>Item 1 subA</a></li>
                                                <li><a href='#'>Item 1 subB</a></li>
                                            </ul>
                                            </li>
                                            <li>
                                            <a href='#'> Item 1 sub</a>
                                            <ul class='menu'>
                                                <li><a href='#'>Item 1 subA</a></li>
                                            </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Item 1B</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Item 2</a>
                                <ul class="menu">
                                    <li><a href="#">Item 2A</a></li>
                                    <li><a href="#">Item 2B</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Item 3</a></li>
                            <li><a href='#'>Item 4</a></li>
                        </ul>
                    <hr>


                    <!-- Grid -->
                    <h2 id="grid" class="docs-heading" data-magellan-target="grid"><a href="#grid"></a>Grid (XY)</h2>
                    <div class="kitchen-sink-grid">
                        <div class="grid-x">
                            <div class="cell">full width cell</div>
                            <div class="cell">full width cell</div>
                        </div>
                        <div class="grid-x">
                            <div class="small-6 cell">6 cells</div>
                            <div class="small-6 cell">6 cells</div>
                        </div>
                        <div class="grid-x">
                            <div class="medium-6 large-4 cell">12/6/4 cells</div>
                            <div class="medium-6 large-8 cell">12/6/8 cells</div>
                        </div>
                    </div>
                    <hr>

                    <!-- Pagination -->
                    <h2 id="pagination" class="docs-heading" data-magellan-target="pagination"><a href="#pagination"></a>Pagination</h2>
                    <ul class="pagination" role="navigation" aria-label="Pagination">
                        <li class="disabled">Previous <span class="show-for-sr">page</span></li>
                        <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                        <li><a href="#" aria-label="Page 2">2</a></li>
                        <li><a href="#" aria-label="Page 3">3</a></li>
                        <li><a href="#" aria-label="Page 4">4</a></li>
                        <li class="ellipsis" aria-hidden="true"></li>
                        <li><a href="#" aria-label="Page 12">12</a></li>
                        <li><a href="#" aria-label="Page 13">13</a></li>
                        <li><a href="#" aria-label="Next page">Next <span class="show-for-sr">page</span></a></li>
                    </ul>
                    <hr>


                    <!-- Reveal -->
                    <h2 id="reveal" class="docs-heading" data-magellan-target="reveal"><a href="#reveal"></a>Reveal</h2>
                    <p><a data-open="exampleModal1">Click me for a basic modal</a></p>
                    <p><a data-toggle="exampleModal8">Click me for a full-screen modal</a></p>

                        <!-- Basic modal -->
                    <div class="reveal" id="exampleModal1" data-reveal>
                        <h2>This is a basic modal</h2>
                        <p class="lead">Using hipster ipsum for dummy text</p>
                        <p>Stumptown direct trade swag hella iPhone post-ironic. Before they sold out blog twee, quinoa forage pour-over microdosing deep v keffiyeh fanny pack. Occupy polaroid tilde, bitters vegan man bun gentrify meggings.</p>
                        <button class="close-button" data-close aria-label="Close reveal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <!-- Table -->
                    <h2 id="table" class="docs-heading" data-magellan-target="table"><a href="#table"></a>Table</h2>
                    <table class="stack">
                        <thead>
                            <tr>
                                <th width="200">Table Header Long Title Test n.1</th>
                                <th>Table Header</th>
                                <th width="150">Table Header</th>
                                <th width="150">Table Header</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Content Goes Here</td>
                                <td>This is longer content Donec id elit non mi porta gravida at eget metus.</td>
                                <td>Content Goes Here</td>
                                <td>Content Goes Here</td>
                            </tr>
                            <tr>
                                <td>Content Goes Here</td>
                                <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
                                <td>Content Goes Here</td>
                                <td>Content Goes Here</td>
                            </tr>
                            <tr>
                                <td>Content Goes Here</td>
                                <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
                                <td>Content Goes Here</td>
                                <td>Content Goes Here</td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>

                    <!-- Tabs -->
                    <h2 id="tabs" class="docs-heading" data-magellan-target="tabs"><a href="#tabs"></a>Tabs</h2>

                    <ul class="tabs" data-responsive-accordion-tabs="accordion medium-tabs" id="example-tabs">
                        <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Tab 1</a></li>
                        <li class="tabs-title"><a href="#panel2">Tab 2</a></li>
                        <li class="tabs-title"><a href="#panel3">Tab 3</a></li>
                        <li class="tabs-title"><a href="#panel4">Tab 4</a></li>
                        <li class="tabs-title"><a href="#panel5">Tab 5</a></li>
                        <li class="tabs-title"><a href="#panel6">Tab 6</a></li>
                    </ul>

                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel is-active" id="panel1">
                            <p>One</p>
                            <p>Check me out! I'm a super cool Tab panel with text content! On medium-down screen sizes, this component will transform into an accordion.</p>
                        </div>
                        <div class="tabs-panel" id="panel2">
                            <p>Two</p>
                            <img class="thumbnail" src="http://placeimg.com/200/200/arch">
                        </div>
                        <div class="tabs-panel" id="panel3">
                            <p>Three</p>
                            <p>Check me out! I'm a super cool Tab panel with text content!</p>
                        </div>
                        <div class="tabs-panel" id="panel4">
                            <p>Four</p>
                            <img class="thumbnail" src="http://placeimg.com/200/200/arch">
                        </div>
                        <div class="tabs-panel" id="panel5">
                            <p>Five</p>
                            <p>Check me out! I'm a super cool Tab panel with text content!</p>
                        </div>
                        <div class="tabs-panel" id="panel6">
                            <p>Six</p>
                            <img class="thumbnail" src="http://placeimg.com/200/200/arch">
                        </div>
                    </div>
                    <hr>
                </article>
            </div>

            <!-- On this page - sidebar nav container -->
            <nav id="kitchen-sink-nav" class="kitchen-sink-nav" data-sticky-container>
                <div class="docs-toc" data-sticky="sidebar" data-anchor="components">
                    <ul class="vertical menu docs-sub-menu" data-magellan>
                        <li class="docs-menu-title">On this page:</li>
                        <li><a href="#accordion">Accordion</a></li>
                        <li><a href="#accordion-menu">Accordion Menu</a></li>
                        <li><a href="#button">Button</a></li>
                        <li><a href="#callout">Callout</a></li>
                        <li><a href="#drilldown-menu">Drilldown Menu</a></li>
                        <li><a href="#dropdown-menu">Dropdown Menu</a></li>
                        <li><a href="#grid">Grid</a></li>
                        <li><a href="#pagination">Pagination</a></li>
                        <li><a href="#reveal">Reveal</a></li>
                        <li><a href="#table">Table</a></li>
                        <li><a href="#tabs">Tabs</a></li>
                    </ul>
                </div>
            </nav>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </div><!-- Close main-grid -->
    </div><!-- Close main-container -->
<?php endwhile; ?>
<?php get_footer();
