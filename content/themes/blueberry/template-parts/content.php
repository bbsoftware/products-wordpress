<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 $post_class = '';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?>>
	<h1>
		<?php the_title(); ?>
	</h1>
</article>
