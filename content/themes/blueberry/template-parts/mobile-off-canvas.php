<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<nav class="mobile-off-canvas-menu off-canvas position-right" id="<?php foundationpress_mobile_menu_id(); ?>" data-off-canvas data-auto-focus="false" role="navigation">
	<div class="off-canvas-close">
		<button aria-label="<?php _e( 'Close Menu', 'theme_textdomain' ); ?>" class="menu-close" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?>">&times;</button>
	</div>

	<?php foundationpress_mobile_nav(); ?>
</nav>

<div class="off-canvas-content" data-off-canvas-content>
